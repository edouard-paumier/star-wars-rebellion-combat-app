# .

## TODO


## Done
* Point-Blank Assault (start): All units in this system have -1 health, to a minimum of 1
* According to my design (start): For each battle during the first combat round, the rebel player rolls 1 fewer red die and 2 fewer black dice
* ION Cannon: during each space battle, step, your opponent rolls 2 fewer red dice
* Better placement of button, take initiative replaced by letting the other fight or something, applied only in the theatre, without changing the aggressor and defender of the fight.
* One in a million (once): Instead of rolling up to 2 of your dice, place them on the table showing the results of your choice
* Target the Star Destroyers (start): During the space battle of each combat round, treat up to 2 of your black hit as red hit