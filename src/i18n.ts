import { createI18n } from 'vue-i18n'
import messages from '@intlify/vite-plugin-vue-i18n/messages'

function chooseLocale (availableLocales: string[]) {
  if (!navigator.languages || !navigator.languages.length) {
    return null
  }

  const compatibleLocales = navigator.languages
    .filter(locale => Object.keys(availableLocales).includes(locale))

  return compatibleLocales.length
    ? compatibleLocales[0]
    : navigator.languages[0]
}

export default createI18n({
  globalInjection: true,
  locale: chooseLocale(Object.keys(messages)) || 'en',
  fallbackLocale: 'en',
  messages
})
