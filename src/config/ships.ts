export type HullType = 'light' | 'heavy'

export interface VulnerableUnitBlueprint {
  name: string
  lightDamage?: number
  heavyDamage?: number
  versatileDamage?: number
  maxHealth: number
  type: HullType
  maxCount: number
}

export interface DeathStarBlueprint {
  name: string
  lightDamage?: number
  heavyDamage: number
  versatileDamage?: number
  maxCount: number
}

export type UnitBlueprint = VulnerableUnitBlueprint | DeathStarBlueprint

export type Faction = 'empire' | 'rebel'
export type Theatre = 'space' | 'ground'

export interface FactionBlueprints {
  space: {
    [id: string]: UnitBlueprint
  }
  ground: {
    [id: string]: UnitBlueprint
  }
}

export type Config = {
  [faction in Faction]: FactionBlueprints
}

const config: Config = {
  rebel: {
    space: {
      'x-wing': {
        name: 'X-Wing',
        lightDamage: 1,
        type: 'light',
        maxHealth: 1,
        maxCount: 8,
      },
      'y-wing': {
        name: 'Y-Wing',
        heavyDamage: 1,
        type: 'light',
        maxHealth: 1,
        maxCount: 12,
      },
      'rebel-transport': {
        name: 'Rebel transport',
        type: 'heavy',
        maxHealth: 2,
        maxCount: 4,
      },
      'corellian-corvette': {
        name: 'Corellian corvette',
        lightDamage: 1,
        heavyDamage: 1,
        type: 'heavy',
        maxHealth: 2,
        maxCount: 4,
      },
      'mon-calamari-cruiser': {
        name: 'Mon Calamari cruiser',
        lightDamage: 1,
        heavyDamage: 2,
        type: 'heavy',
        maxHealth: 4,
        maxCount: 3,
      },
      'u-wing': {
        name: 'U-Wing',
        versatileDamage: 1,
        type: 'light',
        maxHealth: 1,
        maxCount: 6,
      },
      'nebulon-b-frigate': {
        name: 'Nebulon-B frigate',
        versatileDamage: 2,
        type: 'heavy',
        maxHealth: 3,
        maxCount: 4,
      },
    },
    ground: {
      'rebel-trooper': {
        name: 'Rebel trooper',
        lightDamage: 1,
        type: 'light',
        maxHealth: 1,
        maxCount: 21,
      },
      'airspeeder': {
        name: 'Airspeeder',
        lightDamage: 1,
        heavyDamage: 1,
        type: 'heavy',
        maxHealth: 2,
        maxCount: 6,
      },
      'shield-generator': {
        name: 'Shield generator',
        type: 'heavy',
        maxHealth: 3,
        maxCount: 3,
      },
      'ion-cannon': {
        name: 'Ion cannon',
        type: 'heavy',
        maxHealth: 3,
        maxCount: 3,
      },
      'rebel-vanguard': {
        name: 'Rebel vanguard',
        versatileDamage: 1,
        type: 'light',
        maxHealth: 1,
        maxCount: 5,
      },
      'golan-arms-turret': {
        name: 'Golan arms turret',
        versatileDamage: 2,
        type: 'heavy',
        maxHealth: 3,
        maxCount: 4,
      },
    },
  },
  empire: {
    space: {
      'tie-fighter': {
        name: 'Tie fighter',
        lightDamage: 1,
        type: 'light',
        maxHealth: 1,
        maxCount: 24,
      },
      'assault-carrier': {
        name: 'Assault carrier',
        lightDamage: 1,
        heavyDamage: 1,
        type: 'heavy',
        maxHealth: 2,
        maxCount: 8,
      },
      'star-destroyer': {
        name: 'Star destroyer',
        lightDamage: 1,
        heavyDamage: 2,
        type: 'heavy',
        maxHealth: 4,
        maxCount: 8,
      },
      'super-star-destroyer': {
        name: 'Super star destroyer',
        lightDamage: 2,
        heavyDamage: 3,
        type: 'heavy',
        maxHealth: 6,
        maxCount: 2,
      },
      'death-star-under-construction': {
        name: 'Death star under construction',
        type: 'light',
        maxHealth: 4,
        maxCount: 1,
      },
      'death-star': {
        name: 'Death star',
        heavyDamage: 4,
        maxCount: 2,
      },
      'tie-striker': {
        name: 'Tie striker',
        versatileDamage: 1,
        type: 'light',
        maxHealth: 1,
        maxCount: 6,
      },
      'interdictor': {
        name: 'Interdictor',
        versatileDamage: 2,
        type: 'heavy',
        maxHealth: 4,
        maxCount: 3,
      },
    },
    ground: {
      'stormtrooper': {
        name: 'Stormtrooper',
        lightDamage: 1,
        type: 'light',
        maxHealth: 1,
        maxCount: 30,
      },
      'at-st': {
        name: 'AT-ST',
        lightDamage: 1,
        heavyDamage: 1,
        type: 'heavy',
        maxHealth: 2,
        maxCount: 10,
      },
      'at-at': {
        name: 'AT-AT',
        lightDamage: 1,
        heavyDamage: 2,
        type: 'heavy',
        maxHealth: 3,
        maxCount: 4,
      },
      'assault-tank': {
        name: 'Assault tank',
        versatileDamage: 1,
        type: 'heavy',
        maxHealth: 1,
        maxCount: 6,
      },
      'shield-bunker': {
        name: 'Shield bunker',
        type: 'heavy',
        maxHealth: 3,
        maxCount: 3,
      },
    },
  },
}

export default config
