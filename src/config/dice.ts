export type DieFace = 'hit' | 'direct-hit' | 'special' | 'miss'
export type DieType = 'light' | 'heavy' | 'versatile'

type DieSpec = {
  [dieFace in DieFace]?: number
}

type Die = DieFace[]

export type DiceCount = {
  [dieType in DieType]: number
}

export type DiceResult = {
  [dieType in DieType]: DieFace[]
}

export type DiceId = {
  type: DieType
  index: number
}

function createDieArray(spec: DieSpec): Die {
  const array: DieFace[] = Array(6).fill('miss')

  let end = 6
  fillAndMoveCursor('special')
  fillAndMoveCursor('direct-hit')
  fillAndMoveCursor('hit')

  return array

  function fillAndMoveCursor(face: DieFace) {
    const faceCount = spec[face]
    if (faceCount) {
      array.fill(face, end - faceCount, end)
      end -= faceCount
    }
  }
}

const standardDie = createDieArray({
  'hit': 2,
  'direct-hit': 1,
  'special': 1,
})

const diceConfig = {
  light: standardDie,
  heavy: standardDie,
  versatile: createDieArray({
    'direct-hit': 2,
  }),
}

const faceOrderMap = {
  'hit': 0,
  'direct-hit': 1,
  'special': 2,
  'miss': 3,
}

function getRandomDie(die: Die) {
  return die[Math.floor(Math.random() * 6)]
}

function getRandomFaces(die: Die, count: number) {
  return sortDiceFaces(Array(count).fill(die).map(getRandomDie))
}

function sortDiceFaces(faces: DieFace[]) {
  return faces.sort((a, b) => faceOrderMap[a] - faceOrderMap[b])
}

export function getBlankDice(dice: DiceCount): DiceResult {
  return {
    light: Array(dice.light).fill('miss'),
    heavy: Array(dice.heavy).fill('miss'),
    versatile: Array(dice.versatile).fill('miss'),
  }
}

export function throwDice(dice: DiceCount): DiceResult {
  return {
    light: getRandomFaces(diceConfig.light, dice.light),
    heavy: getRandomFaces(diceConfig.heavy, dice.heavy),
    versatile: getRandomFaces(diceConfig.versatile, dice.versatile),
  }
}

export function countNotMiss(dice: DiceResult) {
  return count('light') + count('heavy') + count('versatile')

  function count(type: DieType) {
    return dice[type].filter((face) => face !== 'miss').length
  }
}

export function rollBlankDice(dice: DiceResult) {
  rollDice('light')
  rollDice('heavy')
  rollDice('versatile')

  function rollDice(type: DieType) {
    const diceOfType = dice[type]
    for (const index in diceOfType) {
      if (diceOfType[index] === 'miss') {
        diceOfType[index] = getRandomDie(diceConfig[type])
      }
    }
    sortDiceFaces(diceOfType)
  }
}

export function rerollDice(dice: DiceResult, diceIds: DiceId[]) {
  for (const id of diceIds) {
    if (dice[id.type][id.index]) {
      dice[id.type][id.index] = getRandomDie(diceConfig[id.type])
    }
    sortDiceFaces(dice[id.type])
  }
}

export function turnHeavy(dice: DiceResult, diceIds: DiceId[]) {
  for (const id of diceIds) {
    const [value] = dice[id.type].splice(id.index, 1)
    dice.heavy.push(value)
  }
  sortDiceFaces(dice.heavy)
}

export interface DieEffects {
  damageLight: number
  damageHeavy: number
  damageAny: number
  healLight: number
  healHeavy: number
}

export function createDefaultEffects(): DieEffects {
  return {
    damageLight: 0,
    damageHeavy: 0,
    damageAny: 0,
    healLight: 0,
    healHeavy: 0,
  }
}

export function getDieEffects(dice: DiceResult, prevents: DieEffects) {
  const effects: DieEffects = createDefaultEffects()

  for (const type in dice) {
    applyDieEffects(type as DieType)
  }

  for (const type in prevents) {
    preventEffects(type as keyof DieEffects)
  }

  function applyDieEffects(type: DieType) {
    dice[type]
      .map((face) => getDieEffect(type, face))
      .forEach((effect) => {
        if (effect) {
          effects[effect]++
        }
      })
  }

  function getDieEffect(type: DieType, face: DieFace): keyof DieEffects | null {
    if (type === 'versatile' && face !== 'miss' && face !== 'direct-hit')
      throw new Error('Unexpected face for versatile:' + face)
    switch (face) {
      case 'direct-hit':
        return 'damageAny'
      case 'miss':
        return null
      case 'hit':
        return type === 'light' ? 'damageLight' : 'damageHeavy'
      case 'special':
        return type === 'light' ? 'healLight' : 'healHeavy'
    }
  }

  function preventEffects(type: keyof DieEffects) {
    if (prevents[type] && type in effects) {
      if (effects[type] < prevents[type]) {
        effects[type] = 0
      } else {
        effects[type] -= prevents[type]
      }
    }
  }

  return effects
}
