import { defineStore } from 'pinia'
import {
  createDefaultEffects,
  getDieEffects,
  rerollDice,
  type DieEffects,
  getBlankDice,
  rollBlankDice,
  turnHeavy,
  countNotMiss,
} from '@/config/dice'
import type { DiceId, DiceResult } from '@/config/dice'

import type {
  Config,
  Faction,
  FactionBlueprints,
  DeathStarBlueprint,
  UnitBlueprint,
  VulnerableUnitBlueprint,
  Theatre,
} from '../config/ships'
import config from '../config/ships'

export type HullType = 'light' | 'heavy'

export interface VulnerableUnitState extends VulnerableUnitBlueprint {
  count: number
  destruction: number
  damage: number
  dieEffects: DieEffects
  assignedRemoval: number
}

export interface DeathStarState extends DeathStarBlueprint {
  count: number
  destruction: number
}

export type UnitState = VulnerableUnitState | DeathStarState

export function isVulnerableUnit(unit: UnitState): unit is VulnerableUnitState {
  return (unit as VulnerableUnitState).type !== undefined
}

export function isDeathStarUnit(unit: UnitState): unit is DeathStarState {
  return !isVulnerableUnit(unit)
}

export interface FactionState {
  space: TheatreState
  ground: TheatreState
  tacticStrengths: TacticStrengths
}

export interface EmpireFactionState extends FactionState {
  faction: 'empire'
  specialCards: {
    accordingToMyDesign: boolean
  }
}

export interface RebelFactionState extends FactionState {
  faction: 'rebel'
  specialCards: {
    pointBlankAssault: boolean
    targetStarDestroyers: boolean
  }
}

export type TacticStrengths = {
  [theatre in Theatre]: number
}

export interface TheatreState {
  [id: string]: UnitState
}

export type State = {
  factionA: Faction
  rebel: RebelFactionState
  empire: EmpireFactionState
  aggressor: Faction | null
  initiative: {
    [theatre in Theatre]: Faction | null
  }
  status: Status
  oneInMillionAvailable: boolean
  tutorialMode: boolean
}

interface InitialStatus {
  type: 'initial'
}

type Tactics = {
  [faction in Faction]: Tactic
}

export interface Tactic extends DieEffects {
  removeDamage: boolean
}

function createDefaultTactic(): Tactic {
  return {
    ...createDefaultEffects(),
    removeDamage: false,
  }
}

export interface TacticStatus {
  type: 'tactic'
  theatre: Theatre
  tactics: Tactics
  roundIndex: number
}

export interface BattleRoundStatus {
  roundIndex: number
  theatre: Theatre
  round: Round
  active: Faction
  passive: Faction
  tactics: Tactics
}

export interface BattleRoundPreRollingStatus extends BattleRoundStatus {
  type: 'battle-pre-rolling'
  diceResult: DiceResult
}

export interface BattleRoundRollingStatus extends BattleRoundStatus {
  type: 'battle-rolling'
  diceResult: DiceResult
  rerollAvailable: boolean
  targetStarDestroyersAvailable: boolean
}

export interface BattleRoundAssignmentStatus extends BattleRoundStatus {
  type: 'battle-assignment'
  dieEffects: DieEffects
}

export interface BattleRoundRemovalStatus extends BattleRoundStatus {
  type: 'battle-removal'
  availableRemoval: number
  drawingFire?: string
  drawingGettingDamage?: boolean
}

interface EndStatus {
  type: 'end'
}

export type Status =
  | InitialStatus
  | TacticStatus
  | BattleRoundPreRollingStatus
  | BattleRoundRollingStatus
  | BattleRoundAssignmentStatus
  | BattleRoundRemovalStatus
  | EndStatus

export type Round = 'aggressor' | 'defender'

function clamp(value: number, max: number) {
  return Math.max(0, Math.min(value, max))
}

export function isTacticStatus(status: Status): status is TacticStatus {
  return (status as Status).type === 'tactic'
}

export function isBattleRoundPreRollingStatus(
  status: Status
): status is BattleRoundPreRollingStatus {
  return (status as Status).type === 'battle-pre-rolling'
}

export function isBattleRoundRollingStatus(
  status: Status
): status is BattleRoundRollingStatus {
  return (status as Status).type === 'battle-rolling'
}

export function isBattleRoundAssignmentStatus(
  status: Status
): status is BattleRoundAssignmentStatus {
  return (status as Status).type === 'battle-assignment'
}

export function isBattleRoundRemovalStatus(
  status: Status
): status is BattleRoundRemovalStatus {
  return (status as Status).type === 'battle-removal'
}

export function isBattleRoundStatus(
  status: Status
): status is
  | BattleRoundPreRollingStatus
  | BattleRoundRollingStatus
  | BattleRoundAssignmentStatus
  | BattleRoundRemovalStatus {
  return (
    isBattleRoundPreRollingStatus(status) ||
    isBattleRoundRollingStatus(status) ||
    isBattleRoundAssignmentStatus(status) ||
    isBattleRoundRemovalStatus(status)
  )
}

function createState(config: Config): State {
  const status: Status = { type: 'initial' }
  const state = {
    factionA: 'rebel' as Faction,
    empire: createEmpireFactionState(config.empire),
    rebel: createRebelFactionState(config.rebel),
    aggressor: null as Faction | null,
    initiative: {
      space: null as Faction | null,
      ground: null as Faction | null,
    },
    status,
    oneInMillionAvailable: true,
    tutorialMode: false
  }

  return state

  function createEmpireFactionState(
    faction: FactionBlueprints
  ): EmpireFactionState {
    return {
      faction: 'empire',
      ...createFactionState(faction),
      specialCards: {
        accordingToMyDesign: false,
      },
    }
  }

  function createRebelFactionState(
    faction: FactionBlueprints
  ): RebelFactionState {
    return {
      faction: 'rebel',
      ...createFactionState(faction),
      specialCards: {
        pointBlankAssault: false,
        targetStarDestroyers: false,
      },
    }
  }

  function createFactionState(faction: FactionBlueprints): FactionState {
    return {
      space: createUnitStates(faction.space),
      ground: createUnitStates(faction.ground),
      tacticStrengths: {
        space: 0,
        ground: 0,
      },
    }
  }

  function createUnitStates(units: { [id: string]: UnitBlueprint }): {
    [id: string]: UnitState
  } {
    return Object.fromEntries(
      Object.entries(units).map(([id, unit]) => [id, createUnitState(unit)])
    )
  }

  function createUnitState(unit: UnitBlueprint): UnitState {
    if ((unit as VulnerableUnitState).type !== undefined) {
      return createVulnerableUnitState(unit as VulnerableUnitState)
    } else {
      return createDeathStarUnitState(unit as DeathStarBlueprint)
    }

    function createVulnerableUnitState(
      unit: VulnerableUnitBlueprint
    ): VulnerableUnitState {
      return {
        ...unit,
        count: 0,
        destruction: 0,
        damage: 0,
        dieEffects: createDefaultEffects(),
        assignedRemoval: 0,
      }
    }

    function createDeathStarUnitState(
      unit: DeathStarBlueprint
    ): DeathStarState {
      return {
        ...unit,
        count: 0,
        destruction: 0,
      }
    }
  }
}

export const useFightStore = defineStore({
  id: 'fight',
  state: () => createState(config),

  getters: {
    factionB(): Faction {
      return this.factionA === 'rebel'
        ? 'empire'
        : 'rebel'
    },

    battles(): { [theatre in Theatre]: boolean } {
      const isBattleActive = (theatre: Theatre) =>
        hasUnits(this['rebel'][theatre]) && hasUnits(this['empire'][theatre])

      return {
        space: isBattleActive('space'),
        ground: isBattleActive('ground'),
      }

      function hasUnits(state: TheatreState) {
        return Object.values(state).some((unit) => unit.count)
      }
    },
  },

  actions: {
    swap() {
      this.factionA = this.factionB
    },

    nextFight() {
      const roundIdIncrement = (currentTheatre: Theatre) => {
        if (!this.battles.ground) {
          return 1
        }
        if (!this.battles.space) {
          return 1
        }
        return currentTheatre === 'space' ? 0 : 1
      }

      const nextTheatre = (currentTheatre: Theatre) => {
        if (!this.battles.ground) {
          return 'space'
        }
        if (!this.battles.space) {
          return 'ground'
        }
        return currentTheatre === 'space' ? 'ground' : 'space'
      }

      if (this.status.type === 'end') {
        this.startNewFight()
        return
      }

      if (this.status.type === 'initial') {
        this.endSetup()
        this.startTactic(0, nextTheatre('ground')) // TODO: What if no unit in space ?
        return
      }

      if (this.status.type === 'tactic') {
        this.endTactics(this.status)
        this.startTheatre(
          this.status.roundIndex,
          this.status.theatre,
          'aggressor',
          this.status.tactics
        )
        return
      }

      if (this.status.type === 'battle-pre-rolling') {
        this.roll(this.status)
        return
      }

      if (this.status.type === 'battle-rolling') {
        this.startAssigning(this.status)
        return
      }

      if (
        this.status.type === 'battle-assignment' &&
        this.status.tactics[this.status.passive].removeDamage
      ) {
        this.startRemoval(this.status)
        return
      }

      if (this.status.round === 'aggressor') {
        this.startTheatre(
          this.status.roundIndex,
          this.status.theatre,
          'defender',
          this.status.tactics
        )
        return
      }

      this.applyDamages()

      if (!this.battles.ground && !this.battles.space) {
        this.endCombat()
        return
      }

      this.startTactic(
        this.status.roundIndex + roundIdIncrement(this.status.theatre),
        nextTheatre(this.status.theatre)
      )
    },

    startNewFight() {
      const { 
        oneInMillionAvailable,
        factionA
       } = this
      this.$reset()
      this.oneInMillionAvailable = oneInMillionAvailable
      this.factionA = factionA
    },

    forEachUnit(change: (unit: UnitState) => void, theatre?: Theatre) {
      this.$patch(() => {
        applyFactionChanges(this.rebel)
        applyFactionChanges(this.empire)
      })

      function applyFactionChanges(faction: FactionState) {
        if (theatre) {
          for (const key in faction[theatre]) {
            change(faction[theatre][key])
          }
        } else {
          for (const key in faction.space) {
            change(faction.space[key])
          }
          for (const key in faction.ground) {
            change(faction.ground[key])
          }
        }
      }
    },

    endSetup() {
      if (this.rebel.specialCards.pointBlankAssault) {
        this.forEachUnit((unit) => {
          if (isVulnerableUnit(unit) && unit.maxHealth > 1) {
            unit.maxHealth--
          }
        })
      }
    },

    startTactic(roundIndex: number, theatre: Theatre) {
      this.status = {
        type: 'tactic',
        roundIndex,
        theatre,
        tactics: {
          empire: createDefaultTactic(),
          rebel: createDefaultTactic(),
        },
      }
    },

    endTactics(status: TacticStatus) {
      this.forEachUnit((unit) => {
        unit.count = clamp(unit.count - unit.destruction, unit.maxCount)
        unit.destruction = 0
      }, status.theatre)
    },

    startTheatre(
      roundIndex: number,
      theatre: Theatre,
      round: Round,
      tactics: Tactics
    ) {
      const { active, passive } = this.getFactionsForRound(theatre, round)
      this.status = {
        type: 'battle-pre-rolling',
        roundIndex,
        theatre,
        round,
        active,
        passive,
        tactics,
        diceResult: getBlankDice(
          this.getDiceCount(active, passive, theatre, roundIndex)
        ),
      }

      if (!this.oneInMillionAvailable || active !== 'rebel') {
        this.nextFight()
      }
    },

    getFactionsForRound(
      theatre: Theatre,
      round: Round
    ): { active: Faction; passive: Faction } {
      const initiative = this.initiative[theatre]
      if (!initiative) {
        throw new Error('Cannot get active faction if initiative is not known')
      }
      const otherFaction = initiative === 'empire' ? 'rebel' : 'empire'
      return round === 'aggressor'
        ? { active: initiative, passive: otherFaction }
        : { active: otherFaction, passive: initiative }
    },

    getDiceCount(
      faction: Faction,
      opposing: Faction,
      theatre: Theatre,
      roundIndex: number
    ) {
      const units = this[faction][theatre]
      const dice = { light: 0, heavy: 0, versatile: 0 }
      Object.values(units).forEach((unit) => {
        dice.light += unit.count * (unit.lightDamage || 0)
        dice.heavy += unit.count * (unit.heavyDamage || 0)
        dice.versatile += unit.count * (unit.versatileDamage || 0)
      })

      if (
        opposing === 'empire' &&
        this[opposing].specialCards.accordingToMyDesign &&
        roundIndex === 0
      ) {
        dice.heavy -= 1
        dice.light -= 2
      }

      if (
        opposing === 'rebel' &&
        theatre === 'space' &&
        this[opposing].ground['ion-cannon'].count
      ) {
        dice.heavy -= 2
      }

      dice.light = clamp(dice.light, 5)
      dice.heavy = clamp(dice.heavy, 5)
      dice.versatile = clamp(dice.versatile, 3)

      return dice
    },

    roll(status: BattleRoundPreRollingStatus) {
      if (countNotMiss(status.diceResult)) {
        this.oneInMillionAvailable = false
      }
      rollBlankDice(status.diceResult)
      const targetStarDestroyersAvailable =
        status.active === 'rebel' &&
        this.rebel.specialCards.targetStarDestroyers &&
        status.theatre === 'space'

      const rerollAvailable =
        this[status.active].tacticStrengths[status.theatre] > 0

      this.status = {
        ...status,
        type: 'battle-rolling',
        rerollAvailable,
        targetStarDestroyersAvailable,
      }

      if (!rerollAvailable && !targetStarDestroyersAvailable) {
        this.nextFight()
      }
    },

    reroll(diceIds: DiceId[]) {
      if (
        this.status.type === 'battle-rolling' &&
        this.status.rerollAvailable
      ) {
        rerollDice(this.status.diceResult, diceIds)
        this.status.rerollAvailable = false

        if (!this.status.targetStarDestroyersAvailable) {
          this.nextFight()
        }
      }
    },

    turnHeavy(diceIds: DiceId[]) {
      if (
        this.status.type === 'battle-rolling' &&
        this.status.targetStarDestroyersAvailable
      ) {
        turnHeavy(this.status.diceResult, diceIds)
        this.status.rerollAvailable = false
        this.status.targetStarDestroyersAvailable = false
        this.nextFight()
      }
    },

    startAssigning(status: BattleRoundRollingStatus) {
      const dieEffects = getDieEffects(
        status.diceResult,
        status.tactics[status.passive]
      )
      this.status = {
        ...status,
        type: 'battle-assignment',
        dieEffects,
      }
    },

    startRemoval(status: BattleRoundAssignmentStatus) {
      this.status = {
        ...status,
        type: 'battle-removal',
        ...removalRules(),
      }

      function removalRules() {
        if (status.theatre === 'space') {
          if (status.passive === 'empire') {
            // Energy shield
            return {
              availableRemoval: 2,
            }
          } else {
            // Draw their fire
            return {
              availableRemoval: 2,
              drawingFire: 'nebulon-b-frigate',
              drawingGettingDamage: false,
            }
          }
        } else {
          if (status.passive === 'empire') {
            // Armored position
            return {
              availableRemoval: 3,
              drawingFire: 'shield-bunker',
              drawingGettingDamage: true,
            }
          } else {
            // Planetary shield
            return {
              availableRemoval: 3,
              drawingFire: 'shield-generator',
              drawingGettingDamage: true,
            }
          }
        }
      }
    },

    applyDamages() {
      if (!isBattleRoundAssignmentStatus(this.status)) {
        return
      }
      const theatre = this.status.theatre

      this.forEachUnit(applyUnitChanges, theatre)

      function applyUnitChanges(unit: UnitState) {
        if (!isVulnerableUnit(unit)) {
          return
        }
        applyDamage(unit)
      }

      function applyDamage(unit: VulnerableUnitState) {
        if (unit.count === 0) {
          return
        }

        while (unit.damage >= unit.maxHealth && unit.count > 0) {
          unit.count--
          unit.damage -= unit.maxHealth
        }
        if (unit.count === 0) {
          unit.damage === 0
        }
      }
    },

    changeHealingDieAssignment(unitId: string, assign: boolean) {
      if (!isBattleRoundAssignmentStatus(this.status)) {
        return
      }
      const unit = this[this.status.active][this.status.theatre][unitId]
      if (!unit || !isVulnerableUnit(unit) || unit.damage <= 0) {
        return
      }
      const effect = unit.type === 'light' ? 'healLight' : 'healHeavy'

      this.changeDieAssignment(
        unit,
        effect,
        assign,
        () => (unit.damage += assign ? -1 : +1)
      )
    },

    changeDamagingDieAssignment(unitId: string, assign: boolean) {
      if (!isBattleRoundAssignmentStatus(this.status)) {
        return
      }
      const unit = this[this.status.passive][this.status.theatre][unitId]
      if (!unit || !isVulnerableUnit(unit)) {
        return
      }
      const effect = unit.type === 'light' ? 'damageLight' : 'damageHeavy'
      const preferredEffect = assign ? effect : 'damageAny'
      const alternativeEffect = assign ? 'damageAny' : effect
      const changeDamage = () => (unit.damage += assign ? +1 : -1)

      this.changeDieAssignment(unit, preferredEffect, assign, changeDamage) ||
        this.changeDieAssignment(unit, alternativeEffect, assign, changeDamage)
    },

    changeDieAssignment(
      unit: VulnerableUnitState,
      effectType: keyof DieEffects,
      assign: boolean,
      action: () => void
    ) {
      if (!isBattleRoundAssignmentStatus(this.status)) {
        return
      }

      return assign
        ? transferDieEffect(this.status.dieEffects, unit.dieEffects)
        : transferDieEffect(unit.dieEffects, this.status.dieEffects)

      function transferDieEffect(from: DieEffects, to: DieEffects) {
        if (from[effectType] <= 0) {
          return false
        }
        from[effectType]--
        to[effectType]++
        action()
        return true
      }
    },

    changeRemovalAssignment(unitId: string, assign: boolean) {
      if (
        !isBattleRoundRemovalStatus(this.status) ||
        unitId === this.status.drawingFire
      ) {
        return
      }
      const unit = this[this.status.passive][this.status.theatre][unitId]
      if (!unit || !isVulnerableUnit(unit)) {
        return
      }

      const drawingDamages =
        this.status.drawingFire &&
        this.status.drawingGettingDamage &&
        this[this.status.passive][this.status.theatre][this.status.drawingFire]
      if (drawingDamages && !drawingDamages.count) {
        return
      }

      if (assign) {
        if (this.status.availableRemoval && unit.damage) {
          this.status.availableRemoval--
          unit.assignedRemoval++
          unit.damage--
          if (drawingDamages && isVulnerableUnit(drawingDamages)) {
            drawingDamages.damage++
          }
        }
      } else {
        if (unit.assignedRemoval) {
          this.status.availableRemoval++
          unit.assignedRemoval--
          unit.damage++
          if (drawingDamages && isVulnerableUnit(drawingDamages)) {
            drawingDamages.damage--
          }
        }
      }
    },

    endCombat() {
      this.status = { type: 'end' }
    },
  },
})
